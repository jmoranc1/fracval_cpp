[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

# PyFracVAL

FracVAL is an open source project intended to simulate fractal-like agglomerates of nanoparticles. This is a C++/Python version of the original (Fortran) code published in
Computer Physics Communications:

* Morán, J., Fuentes, A., Liu, F., & Yon, J. (2019). FracVAL: An improved tunable algorithm of cluster–cluster aggregation for generation of fractal structures formed by polydisperse primary particles. Computer Physics Communications, 239, 225-237. ⟨[10.1016/j.cpc.2019.01.015](https://doi.org/10.1016/j.cpc.2019.01.015)⟩

The description of the hollow or filled raspberry cluster generation is found in:
* Morán, J., Yon, J., Henry, C., & Kholghy, M. R. (2023). Approximating the van der Waals interaction potentials between agglomerates of nanoparticles. Advanced Powder Technology, 34(12), 104269. ⟨[10.1016/j.apt.2023.104269](https://doi.org/10.1016/j.apt.2023.104269)⟩

You may find an introductory presentation to FracVAL, an user-manual of the Fortran version here:
* Presentation: ⟨[10.13140/RG.2.2.34701.59368](https://doi.org/10.13140/RG.2.2.34701.59368)⟩
* User-manual: ⟨[10.13140/RG.2.2.17240.32007](https://doi.org/10.13140/RG.2.2.17240.32007)⟩

## Why developing this new version?

This new version of FracVAL intends to combine the best of C++ (object-oriented, computationally efficient, etc.) and Python (ease to use, has many libraries such as matplotlib, pandas, etc. useful for analysis and post-treatment). The idea is to compile FracVAL from Python, this is not mandatory but highly recomended. Please see this [presentation](https://gitlab.com/jmoranc1/fracval_cpp/-/blob/master/documents/JMoran_FracVAL_2021.pdf) for additional details.

## Installation

Download the current version of the code

    git clone git@gitlab.com:jmoranc1/fracval_cpp.git
    
Compile the code

    mkdir build
    cd build
    cmake ..
    make all

## Update

Go to the "fracval_cpp" folder and type in the terminal:

    git pull
    
Then, recompile the new version (you may first delete all the files within the "build" folder).

## Python

Using Python for post-processing is not mandatory but highly recomended. To use it, you should use the virtual environment automatically created during compilation. Activate the virtual environment by typing in a terminal,

    source venv/bin/activate

Once activated you can compile python codes or use jupyter notebook by typing in the terminal,

    jupyter notebook


## Examples

Sample scripts are provided in the folder: [examples](https://gitlab.com/jmoranc1/fracval_cpp/-/tree/master/examples)

## License

FracVAL is an open-source package, this mean you can use or modify it under the terms and conditions of the GPL-v3 licence. You should have received a copy along with this package, if not please refer to [https://www.gnu.org/licenses/](https://www.gnu.org/licenses/).

## Please cite us!

If you use some or all the codes provided in this repository you are kindly asked to cite our work:
* FracVAL particle-cluster or cluster-cluster agglomeration codes:
    Morán, J., Fuentes, A., Liu, F., & Yon, J. (2019). FracVAL: An improved tunable algorithm of cluster–cluster aggregation for generation of fractal structures formed by polydisperse primary particles. Computer Physics Communications, 239, 225-237. ⟨[10.1016/j.cpc.2019.01.015](https://doi.org/10.1016/j.cpc.2019.01.015)⟩
* FracVAL raspberry clusters:
    Morán, J., Yon, J., Henry, C., & Kholghy, M. R. (2023). Approximating the van der Waals interaction potentials between agglomerates of nanoparticles. Advanced Powder Technology, 34(12), 104269. ⟨[10.1016/j.apt.2023.104269](https://doi.org/10.1016/j.apt.2023.104269)⟩

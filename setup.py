#!/usr/bin/env python3
# coding=utf-8
"""Compile and install pyfracval"""
import sys
from subprocess import run, CalledProcessError

from setuptools import setup as setup
from setuptools import Extension as Extension

setup()

# noinspection PyPep8
import numpy as np

fracval = Extension(name='pyfracval.fracval.fracval_main_wrapper',
                     sources=['pyfracval/fracval/fracval_main_wrapper.pyx',
                     'src/src/main.cpp','src/src/pca.cpp','src/src/tools.cpp',
                     'src/src/sphere.cpp','src/src/aggregate.cpp','src/src/IO.cpp',
                     'src/src/exceptions.cpp','src/src/cca.cpp'],
                     include_dirs=[np.get_include(),'include',
                     'src/src','external/external_bin/inipp/include'],
                     extra_compile_args=["-fopenmp", "-O3"],
                     extra_link_args=["-fopenmp"],
                     language="c++")
# noinspection PyUnusedName
fracval.cython_c_in_temp = True

setup(ext_modules=[fracval])

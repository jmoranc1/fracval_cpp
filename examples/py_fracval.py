#    fracval_cpp: A code to simulate the agglomeration of aerosol particles
#    Copyright (C) 2021  José Morán
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

import numpy as np
import pandas as pd

fracval_available = False

try:
    from pyfracval import fracval_pca_cluster
    from pyfracval import fracval_cca_cluster
    from pyfracval import Plot_3d
    from pyfracval import Generate_filled_rapberry,Generate_hollow_raspberry
    fracval_available = True
except:
    print("pyfracval not available")

def filled_raspberry_cluster(n_shells,Dp_geo=1,z_ad=0, use_pandas=False):
    """
    Generate a 3-dimensional array of spheres forming a
    raspberry shape.
    
    Parameters
    ----------
    n_shells : int
        The number of shells forming the raspberry.
    Dp_geo : float
        Geometric mean primary particle diameter.
    z_ad : int
        Add additional z-planes (becareful it lead to overlapping
        between primary spheres).
    Returns
    -------
    spheres: Pandas DataFrame
        This dataframe contains the following columns [x,y,z,r]
    """
    if (fracval_available != True):
        return 
    else:
        units = Dp_geo/2
        spheres = Generate_filled_rapberry(n_shells,z_ad);
        spheres = spheres * units;
        if (use_pandas):
            spheres = spheres.values;
    return spheres

def hollow_raspberry_cluster(n_shells,Dp_geo=1,z_ad=0, use_pandas=False):
    """
    Generate a 3-dimensional array of spheres forming a
    raspberry shape.
    
    Parameters
    ----------
    n_shells : int
        The number of shells forming the raspberry.
    Dp_geo : float
        Geometric mean primary particle diameter.
    z_ad : int
        Add additional z-planes (becareful it lead to overlapping
        between primary spheres).
    Returns
    -------
    spheres: Pandas DataFrame
        This dataframe contains the following columns [x,y,z,r]
    """
    if (fracval_available != True):
        return 
    else:
        units = Dp_geo/2
        spheres = Generate_hollow_raspberry(n_shells,z_ad);
        spheres = spheres * units;
        if (use_pandas):
            spheres = spheres.values;
    return spheres

def cca_cluster(Np, Df, kf, Dp_geo, Dp_gsd =1.0, tol_ov=1e-06, Np_subcl_perc=0.1, show_error_warnings=False, use_pandas=False):
    """
    Generate an aggregate of spherical primary particles
    using cluster-cluster aggregation approach.
    
    Parameters
    ----------
    Np : int
        The number of primary particles constituing the
        aggregate.
    Df : float
        Fractal dimension.
    kf : float
        Fractal prefactor.
    Dp_geo : float
        Geometric mean primary particle diameter.
    Dp_gsd : float
        Geometric standard deviation (=1 by default).
    tol_ov : float
        Parameter tolerance to overlapping (=1e-06
        by default).
    Np_subcl_perc : float
        Percentage of Np used to build initial sub-
        clusters by PCA (=0.1 by default).
    show_error_warnings : bool
        Decide wether to show all outputs of the code
        (errors and warning) or not.
    use_pandas : bool
        Decide if the output is given as a numpy array 
        or a Pandas dataframe (False by default).
    Returns
    -------
    spheres: Pandas DataFrame (when use_pandas=True)
        This dataframe contains the following columns [x,y,z,r].
    spheres: Numpy array (when use_pandas=False)
        This array contains the following columns [x,y,z,r].
    """
    if (fracval_available != True):
        return 
    else:
        units = Dp_geo/2
        asd = fracval_cca_cluster(Np, Df, kf, 1, Dp_gsd, tol_ov, Np_subcl_perc, show_error_warnings)
        spheres = {
            "x" : [asd[0] * units],
            "y" : [asd[1] * units],
            "z" : [asd[2] * units],
            "r" : [asd[3] * units]
        }
        if (use_pandas):
            spheres = pd.DataFrame(spheres)
    return spheres

def pca_cluster(Np, Df, kf, Dp_geo, Dp_gsd =1.0, tol_ov=1e-06, show_error_warnings=False, use_pandas=False):
    """
    Generate an aggregate of spherical primary particles
    using cluster-cluster aggregation approach.
    
    Parameters
    ----------
    Np : int
        The number of primary particles constituing the
        aggregate.
    Df : float
        Fractal dimension.
    kf : float
        Fractal prefactor.
    Dp_geo : float
        Geometric mean primary particle diameter.
    Dp_gsd : float
        Geometric standard deviation (=1 by default).
    tol_ov : float
        Parameter tolerance to overlapping (=1e-06
        by default).
    show_error_warnings : bool
        Decide wether to show all outputs of the code
        (errors and warning) or not.
    use_pandas : bool
        Decide if the output is given as a numpy array 
        or a Pandas dataframe (False by default).
    Returns
    -------
    spheres: Pandas DataFrame (when use_pandas=True)
        This dataframe contains the following columns [x,y,z,r].
    spheres: Numpy array (when use_pandas=False)
        This array contains the following columns [x,y,z,r].
    """
    if (fracval_available != True):
        return 
    else:
        units = Dp_geo/2
        asd = fracval_pca_cluster(Np, Df, kf, 1, Dp_gsd, tol_ov, show_error_warnings)
        spheres = {
            "x" : [asd[0] * units],
            "y" : [asd[1] * units],
            "z" : [asd[2] * units],
            "r" : [asd[3] * units]
        }
        if (use_pandas):
            spheres = pd.DataFrame(spheres)
    return spheres

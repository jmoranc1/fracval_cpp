/*
    fracval_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "cca.hpp"

using namespace std;

namespace fracval
{
    CCA::CCA() : Df(0.0),
                 kf(0.0),
                 Dpp_geo(0.0),
                 Dpp_gsd(0.0),
                 tol_ov(0.0),
                 Percent_Np_subcl(0.10),
                 Npp(0),
                 Np_subcl(0),
                 cca_not_able(false),
                 show_error_warnings(false),
                 Gamma_cc(0.0),
                 theta_a(0.0),
                 x0(0.0),
                 y0(0.0),
                 z0(0.0),
                 r0(0.0),
                 current_overlapping(0.0),
                 Gamma_real(false),
                 Number_clusters(0),
                 current_available_aggregates(0),
                 selected_cluster_1(0),
                 selected_cluster_2(0)
    {
    }

    /********************************************************************************
    * Generate an aggregate by cluster-cluster agglomeration
    * Input params: Npp,Df,kf,Rpp_geo,Rpp_gsd
    * Return: the sphere list
    ********************************************************************************/
    std::vector<Sphere> CCA::Create_cca_cluster(const size_t Npp_input,
                                                const double Df_input,
                                                const double kf_input,
                                                const double Dpp_geo_input,
                                                const double Dpp_gsd_input,
                                                const double tol_ov_input,
                                                const double Percent_Np_subcl_input)
    {
        // 0. Save parameters
        Npp = Npp_input;
        Df = Df_input;
        kf = kf_input;
        Dpp_geo = Dpp_geo_input;
        Dpp_gsd = Dpp_gsd_input;
        tol_ov = tol_ov_input;
        Percent_Np_subcl = Percent_Np_subcl_input;

        return Create_cca_cluster();
    }

    std::vector<Sphere> CCA::Create_cca_cluster()
    {
        // Initialize monomers list
        std::vector<Sphere> monomers;
        monomers.resize(Npp);
        for (size_t i = 0; i < Npp; i++)
        {
            monomers[i] = Sphere();
        };

        // Check input parameters
        if (check_input_params_cca(Npp, Df, kf, Dpp_geo, Dpp_gsd, tol_ov, Percent_Np_subcl))
        {
            Error(ErrorCodes::INPUT_ERROR, show_error_warnings);
            cca_not_able = true;
            return monomers;
        }

        // Number of primary particles per sub-cluster
        Np_subcl = Initial_Np_subclusters();

        // Create list of aggregates
        std::vector<Aggregate> aggregates;

        // Create sub-clusters by PCA
        if (PCA_subclusters(monomers, aggregates))
        {
            Error(ErrorCodes::PCA_SUBCLUSTERS_NA, show_error_warnings);
            return monomers;
        }

        // Iteratively agglomerate clusters
        while (Number_clusters > 1)
        {
            std::vector<Sphere> monomers_next = Copy_monomers_list(monomers);
            current_available_aggregates = Number_clusters;

            // Agglomerate one generation of clusters
            size_t total_clusters_made(0);
            while ((current_available_aggregates > 1) && (!cca_not_able))
            {
                // Randomly select a pair of subclusters
                Find_new_pair_subclusters(aggregates);

                // Agglomerate the selected pair of subclusters
                cca_not_able = Agglomerate_selected_pair_subclusters(aggregates, monomers_next);
                total_clusters_made++;
            }

            if(cca_not_able)
            {
                Error(ErrorCodes::CCA_NA, show_error_warnings);
                return monomers;
            }

            monomers = Copy_monomers_list(monomers_next);
            Number_clusters -= total_clusters_made;
        }

        return monomers;
    }

    /********************************************************************************
     * Agglomerate two subclusters
    ********************************************************************************/
    bool CCA::Agglomerate_selected_pair_subclusters(std::vector<Aggregate> &aggregates,
                                                    std::vector<Sphere> &monomers_next)
    {
        Aggregate aggregate_1, aggregate_2;
        aggregate_1.Aggregate_copy(aggregates[selected_cluster_1]);
        aggregate_2.Aggregate_copy(aggregates[selected_cluster_2]);

        Aggregate aggregate_next = New_newt_aggregate(aggregate_1, aggregate_2);
        Gamma_calculation(aggregate_next, aggregate_1, aggregate_2);

        return false;
    }

    /********************************************************************************
     * Define properties of Next aggregate
    ********************************************************************************/
    Aggregate CCA::New_newt_aggregate(const Aggregate aggregate_1,
                                      const Aggregate aggregate_2)
    {
        Aggregate aggregate_next;
        aggregate_next.Npp = aggregate_1.Npp + aggregate_2.Npp;
        aggregate_next.Volume = aggregate_1.Volume + aggregate_2.Volume;
        aggregate_next.rg = 0.5 * Dpp_geo * std::pow(static_cast<double>(aggregate_next.Npp) / kf, 1.0 / Df);

        return aggregate_next;
    }

    /********************************************************************************
    * Determine the value of the gamma distance between clusters
    ********************************************************************************/
    void CCA::Gamma_calculation(Aggregate &aggregate_next,
                                const Aggregate aggregate_1,
                                const Aggregate aggregate_2)
    {
        double m1 = aggregate_1.Volume;
        double m2 = aggregate_2.Volume;
        double m3 = aggregate_next.Volume;

        double rg1 = aggregate_1.rg;
        double rg2 = aggregate_2.rg;
        double rg3 = aggregate_next.rg;

        double temp = std::pow(m3, 2) * std::pow(rg3, 2) - m3 * (m1 * std::pow(rg1, 2) + m2 * std::pow(rg2, 2));

        if (temp > 0.0)
        {
            Gamma_cc = std::sqrt(temp / (m1 * m2));
            Gamma_real = true;
        }
        else
        {
            Gamma_cc = 0.0;
            Gamma_real = false;
        }

        aggregate_next.gamma_rmax = Gamma_cc / (aggregate_next.rmax + 0.5 * Dpp_geo);
    }

    /********************************************************************************
     * Determine the number of initial Np per sub-clusters
    ********************************************************************************/
    size_t CCA::Initial_Np_subclusters()
    {
        size_t new_Np_subcl = static_cast<size_t>(Npp * Percent_Np_subcl);
        if (new_Np_subcl < Np_subcl_min)
        {
            new_Np_subcl = Np_subcl_min;
        }
        else if (new_Np_subcl > Np_subcl_max)
        {
            new_Np_subcl = Np_subcl_max;
        }
        return new_Np_subcl;
    }

    /********************************************************************************
     * Generate sub-clusters by PCA
    ********************************************************************************/
    bool CCA::PCA_subclusters(std::vector<Sphere> &monomers,
                              std::vector<Aggregate> &aggregates)
    {
        // The number of sub-clusters
        Number_clusters = std::floor(static_cast<double>(Npp) / static_cast<double>(Np_subcl));

        size_t total_monomers(0);
        for (size_t i = 0; i < Number_clusters; i++)
        {
            if (new_PCA_cluster(total_monomers, Np_subcl, monomers, i, aggregates))
            {
                cca_not_able = true;
                return true;
            }
            total_monomers += Np_subcl;
        }

        if (Npp % Np_subcl != 0)
        {
            size_t Npp_last = Npp - Number_clusters * Np_subcl;

            if (new_PCA_cluster(total_monomers, Npp_last, monomers, Number_clusters, aggregates))
            {
                cca_not_able = true;
                return true;
            }
            Number_clusters++;
        }

        return false;
    }

    bool CCA::new_PCA_cluster(const size_t total_monomers,
                              const size_t Np_new,
                              std::vector<Sphere> &monomers,
                              const size_t id_new,
                              std::vector<Aggregate> &aggregates)
    {
        PCA pca;
        std::vector<Sphere> new_monomers;
        new_monomers.resize(Np_new);
        size_t trial(0);
        while ((!pca.pca_not_able) && (trial < Total_trials_PCA_subcl))
        {
            new_monomers = pca.Create_pca_cluster(Np_new, Df, kf,
                                                  Dpp_geo, Dpp_gsd,
                                                  tol_ov);
            trial++;
        }
        if (!pca.pca_not_able)
        {
            for (size_t j = 0; j < Np_new; j++)
            {
                new_monomers[j].Set_agglomerate_id(id_new);
                monomers[total_monomers + j].Copy_sphere(new_monomers[j]);
            }
            // Create new aggregate
            Aggregate new_aggregate;
            new_aggregate.Initialize(Np_new, Dpp_geo, kf, Df, new_monomers);
            new_aggregate.idx_start_end_monomers = {total_monomers, total_monomers + Np_new - 1};
            new_aggregate.agglomerate_id = id_new;
            aggregates.push_back(new_aggregate);
            return false;
        }
        return true;
    }

    std::vector<Sphere> CCA::Copy_monomers_list(const std::vector<Sphere> old_monomers)
    {
        std::vector<Sphere> new_monomers;
        new_monomers.resize(old_monomers.size());

        for (size_t i = 0; i < old_monomers.size(); i++)
        {
            new_monomers[i].Copy_sphere(old_monomers[i]);
        }

        return new_monomers;
    }

    void CCA::Find_new_pair_subclusters(std::vector<Aggregate> &aggregates)
    {
        size_t cluster_1_idx = Random_integer(current_available_aggregates);
        size_t cluster_2_idx = Random_integer(current_available_aggregates - 1);

        current_available_aggregates -= 2;

        bool found(false), found1(false), found2(false);
        size_t i(0), counter(0);
        while ((found == false) && (i < Number_clusters))
        {
            if (aggregates[i].available)
            {
                if (cluster_1_idx == counter)
                {
                    found1 = true;
                    selected_cluster_1 = i;
                    aggregates[i].available = false;
                }
                if (cluster_2_idx == counter)
                {
                    found2 = true;
                    selected_cluster_2 = i;
                    aggregates[i].available = false;
                }
                found = found1 && found2;
                counter++;
            }
            i++;
        }
    }
}

/*
    fracval_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "exceptions.hpp"

namespace fracval
{
    void Error(const ErrorCodes error_code,
               const bool show_error_warnings)
    {
        if (show_error_warnings)
        {
            if (error_code == ErrorCodes::INPUT_ERROR)
            {
                std::cout << "ERROR, Invalid input parameters " << std::endl;
            }
            if (error_code == ErrorCodes::CANDIDATES_DEPLETED)
            {
                std::cout << "Candidates depleted " << std::endl;
            }
            if (error_code == ErrorCodes::GAMMA_TOO_LARGE)
            {
                std::cout << "Gamma value is too large (check Df/kf used) " << std::endl;
            }
            if (error_code == ErrorCodes::UNKNOWN_ERROR)
            {
                std::cout << "Unknown error " << std::endl;
            }
            if (error_code == ErrorCodes::PCA_SUBCLUSTERS_NA)
            {
                std::cout << "pca subclusters not able" << std::endl;
            }
            if (error_code == ErrorCodes::CCA_NA)
            {
                std::cout << "cca not able" << std::endl;
            }
        }
    }

    /********************************************************************************
    * Check if input parameters are valid or not
    ********************************************************************************/
    bool check_input_params_pca(const size_t Npp_input,
                                const double Df_input,
                                const double kf_input,
                                const double Dpp_geo_input,
                                const double Dpp_gsd_input,
                                const double tol_ov_input)
    {
        if ((Df_input < 1.0) || (Df_input > 3.0))
        {
            return true;
        }

        if (Npp_input < 1)
        {
            return true;
        }

        if (Dpp_gsd_input < 0.9999999999999)
        {
            return true;
        }
        return false;
    }
    bool check_input_params_cca(const size_t Npp_input,
                                const double Df_input,
                                const double kf_input,
                                const double Dpp_geo_input,
                                const double Dpp_gsd_input,
                                const double tol_ov_input,
                                const double Percent_Np_subcl_input)
    {
        if (check_input_params_pca(Npp_input, Df_input, kf_input, Dpp_geo_input, Dpp_gsd_input, tol_ov_input))
        {
            return true;
        }

        if ((Percent_Np_subcl_input < 0.0) || (Percent_Np_subcl_input > 1.0))
        {
            return true;
        }
        return false;
    }
}
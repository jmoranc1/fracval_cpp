/*
    fracval_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "main.hpp"
using namespace std;

int main(int argc,
         char *argv[])
{
    std::cout << std::endl;
    std::cout << "fracval_cpp  Copyright (C) 2021 Jose Moran" << std::endl;
    std::cout << "  This program comes with ABSOLUTELY NO WARRANTY" << std::endl;
    std::cout << "  This is free software, and you are welcome to redistribute it" << std::endl;
    std::cout << "  under certain conditions" << std::endl;
    std::cout << std::endl;
    if (argc <= 1)
    {
        std::cout << "Missing argument: input parameter file." << std::endl;
        return 1;
    }
    // Read input parameters
    fracval::Input_Output IO;
    IO.Read_input_parameters(argv[1]);

    if (IO.method == fracval::Methods::PC_A)
    {
        fracval::PCA pca;
        size_t np = Initialize_pca(IO, pca);
        double X[np];
        double Y[np];
        double Z[np];
        double R[np];
        fracval::pca_cluster(pca, X, Y, Z, R);
    }

    if (IO.method == fracval::Methods::CC_A)
    {
        fracval::CCA cca;
        size_t np = Initialize_cca(IO, cca);
        double X[np];
        double Y[np];
        double Z[np];
        double R[np];
        fracval::cca_cluster(cca, X, Y, Z, R);
    }

    
    return 0;
}

namespace fracval
{
    /********************************************************************************
    * Create a cluster by PCA
    ********************************************************************************/
    void pca_cluster(const size_t Npp_input,
                     const double Df_input,
                     const double kf_input,
                     const double Dpp_geo_input,
                     const double Dpp_gsd_input,
                     const double tol_ov_input,
                     const bool show_error_warnings_input,
                     double X[], double Y[], double Z[], double R[])
    {
        Seed_Random();
        PCA pca;
        pca.show_error_warnings = show_error_warnings_input;
        std::vector<Sphere> monomers = pca.Create_pca_cluster(Npp_input,
                                                              Df_input,
                                                              kf_input,
                                                              Dpp_geo_input,
                                                              Dpp_gsd_input,
                                                              tol_ov_input);
        if (pca.pca_not_able)
        {
            if (pca.show_error_warnings) std::cout << "PCA not able" << std::endl;

            for (size_t i = 0; i < Npp_input; i++)
            {
                X[i] = 0.0;
                Y[i] = 0.0;
                Z[i] = 0.0;
                R[i] = 0.0;
            }
        }
        else
        {
            if (pca.show_error_warnings) std::cout << "Finished successfully" << std::endl;

            for (size_t i = 0; i < Npp_input; i++)
            {
                X[i] = monomers[i].x;
                Y[i] = monomers[i].y;
                Z[i] = monomers[i].z;
                R[i] = monomers[i].Radius;
            }
        }
    }

    /********************************************************************************
    * Create a cluster by PCA
    ********************************************************************************/
    void pca_cluster(PCA pca,
                     double X[],
                     double Y[],
                     double Z[],
                     double R[])
    {
        Seed_Random();
        std::vector<Sphere> monomers = pca.Create_pca_cluster();

        if (pca.pca_not_able)
        {
            if (pca.show_error_warnings) std::cout << "PCA not able" << std::endl;

            for (size_t i = 0; i < pca.Npp; i++)
            {
                X[i] = 0.0;
                Y[i] = 0.0;
                Z[i] = 0.0;
                R[i] = 0.0;
            }
        }
        else
        {
            if (pca.show_error_warnings) std::cout << "Finished successfully" << std::endl;

            for (size_t i = 0; i < pca.Npp; i++)
            {
                X[i] = monomers[i].x;
                Y[i] = monomers[i].y;
                Z[i] = monomers[i].z;
                R[i] = monomers[i].Radius;
            }
        }
    }

    /********************************************************************************
    * Create a cluster by CCA
    ********************************************************************************/
    void cca_cluster(const size_t Npp_input,
                     const double Df_input,
                     const double kf_input,
                     const double Dpp_geo_input,
                     const double Dpp_gsd_input,
                     const double tol_ov_input,
                     const double Percent_Np_subcl_input,
                     const bool show_error_warnings_input,
                     double X[], double Y[], double Z[], double R[])
    {
        Seed_Random();
        CCA cca;
        cca.show_error_warnings = show_error_warnings_input;
        std::vector<Sphere> monomers = cca.Create_cca_cluster(Npp_input,
                                                              Df_input,
                                                              kf_input,
                                                              Dpp_geo_input,
                                                              Dpp_gsd_input,
                                                              tol_ov_input,
                                                              Percent_Np_subcl_input);
        if (cca.cca_not_able)
        {
            if (cca.show_error_warnings) std::cout << "CCA not able" << std::endl;

            for (size_t i = 0; i < Npp_input; i++)
            {
                X[i] = 0.0;
                Y[i] = 0.0;
                Z[i] = 0.0;
                R[i] = 0.0;
            }
        }
        else
        {
            if (cca.show_error_warnings) std::cout << "Finished successfully" << std::endl;

            for (size_t i = 0; i < Npp_input; i++)
            {
                X[i] = monomers[i].x;
                Y[i] = monomers[i].y;
                Z[i] = monomers[i].z;
                R[i] = monomers[i].Radius;
            }
        }
    }

    /********************************************************************************
    * Create a cluster by CCA
    ********************************************************************************/
    void cca_cluster(CCA cca,
                     double X[],
                     double Y[],
                     double Z[],
                     double R[])
    {
        Seed_Random();
        std::vector<Sphere> monomers = cca.Create_cca_cluster();

        if (cca.cca_not_able)
        {
            if (cca.show_error_warnings) std::cout << "CCA not able" << std::endl;

            for (size_t i = 0; i < cca.Npp; i++)
            {
                X[i] = 0.0;
                Y[i] = 0.0;
                Z[i] = 0.0;
                R[i] = 0.0;
            }
        }
        else
        {
            if (cca.show_error_warnings) std::cout << "Finished successfully" << std::endl;

            for (size_t i = 0; i < cca.Npp; i++)
            {
                X[i] = monomers[i].x;
                Y[i] = monomers[i].y;
                Z[i] = monomers[i].z;
                R[i] = monomers[i].Radius;
            }
        }
    }

    /********************************************************************************
    * Seed (pseudo) random numbers
    ********************************************************************************/
    void Seed_Random()
    {
        time_t t;
        time(&t);
        srand(uint(t));
        //srand(0);
    }

    /********************************************************************************
    * Initialize particle-cluster aggregation class based on IO data
    ********************************************************************************/
    size_t Initialize_pca(const Input_Output IO,
                          PCA &pca)
    {
        pca.Npp = IO.Npp_input;
        pca.Df = IO.Df_input;
        pca.kf = IO.kf_input;
        pca.Dpp_geo = IO.Dpp_geo_input;
        pca.Dpp_gsd = IO.Dpp_gsd_input;
        pca.tol_ov = IO.tol_ov_input;
        pca.show_error_warnings = IO.show_error_warning;

        return pca.Npp;
    }
    size_t Initialize_cca(const Input_Output IO,
                          CCA &cca)
    {
        cca.Npp = IO.Npp_input;
        cca.Df = IO.Df_input;
        cca.kf = IO.kf_input;
        cca.Dpp_geo = IO.Dpp_geo_input;
        cca.Dpp_gsd = IO.Dpp_gsd_input;
        cca.tol_ov = IO.tol_ov_input;
        cca.Percent_Np_subcl = IO.Percent_Np_subcl_input;
        cca.show_error_warnings = IO.show_error_warning;
        
        return cca.Npp;
    }
}

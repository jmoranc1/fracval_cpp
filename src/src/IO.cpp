/*
    fracval_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <boost/filesystem.hpp>
#include "IO.hpp"

namespace fracval
{
    Input_Output::Input_Output() : Npp_input(0),
                                   Df_input(1.78),
                                   kf_input(1.3),
                                   Dpp_geo_input(1.0),
                                   Dpp_gsd_input(1.0),
                                   tol_ov_input(1e-06),
                                   Percent_Np_subcl_input(0.1),
                                   show_error_warning(false),
                                   method(Methods::UNKNOWN_METHOD)
    {
    }
    void Input_Output::Read_input_parameters(const std::string &input_parameters_file)
    {
        std::string method_str;
        // Read the config file
        inipp::Ini<char> ini;
        std::ifstream is(input_parameters_file);
        ini.parse(is);
        ini.interpolate();
        // Aggregate
        inipp::extract(ini.sections["Aggregate"]["Npp"], Npp_input);
        inipp::extract(ini.sections["Aggregate"]["Df"], Df_input);
        inipp::extract(ini.sections["Aggregate"]["kf"], kf_input);
        // Primary particles
        inipp::extract(ini.sections["Primary_particles"]["Dpp_geo"], Dpp_geo_input);
        inipp::extract(ini.sections["Primary_particles"]["Dpp_gsd"], Dpp_gsd_input);
        // Simulation
        inipp::extract(ini.sections["Simulation"]["tol_ov"], tol_ov_input);
        inipp::extract(ini.sections["Simulation"]["method"], method_str);
        if (method_str != "")
        {
            method = resolve_method(method_str);
        }
        inipp::extract(ini.sections["Simulation"]["show_error_warning"], show_error_warning);
        inipp::extract(ini.sections["Simulation"]["Percent_Np_subcl_input"], Percent_Np_subcl_input);

        if (show_error_warning)
        {
            std::cout << "Read parameters from: " << input_parameters_file << std::endl;
            print();
            std::cout << "Successfully read parameters" << std::endl;
        }
    }

    Methods Input_Output::resolve_method(const std::string &method_str)
    {
        auto it = std::find(METHODS_STR.begin(),
                            METHODS_STR.end(),
                            method_str);
        if (it != METHODS_STR.end())
        {
            return Methods(std::distance(METHODS_STR.begin(), it));
        }
        return UNKNOWN_METHOD;
    }

    void Input_Output::print()
    {
        std::cout << " IO properties   " << std::endl;
        std::cout << "    Method                : " << METHODS_STR[method] << std::endl;
        std::cout << "    Np                    : " << Npp_input << std::endl;
        std::cout << "    Df                    : " << Df_input << std::endl;
        std::cout << "    kf                    : " << kf_input << std::endl;
        std::cout << "    Dpp_geo               : " << Dpp_geo_input << std::endl;
        std::cout << "    Dpp_gsd               : " << Dpp_gsd_input << std::endl;
        std::cout << "    tol_ov                : " << tol_ov_input << std::endl;
        std::cout << "    show_error_warning    : " << show_error_warning << std::endl;
        std::cout << "    Percent_Np_subcl      : " << Percent_Np_subcl_input << std::endl;
    }
}
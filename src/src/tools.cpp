/*
    fracval_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "tools.hpp"
using namespace std;

namespace fracval{


double rand_01() {
    return double(rand())/(RAND_MAX);
}

void Random_point_sphere(double &theta, double &phi) {
    double u = rand_01();
    double v = rand_01();

    theta = 2.*pi*u;
    phi = std::acos(2.*v-1.);
}

/********************************************************************************
* Returns a random integer between 0 and (n_max-1)
********************************************************************************/
size_t Random_integer(const size_t n_max){
    return rand() % n_max;
}

/********************************************************************************
* Lognormally distributed random monomers
********************************************************************************/
__attribute((const)) double inverfc(const double pp){
        double x,t,ppp;
        if (pp >= 2.0){ return -100.;}
        if (pp <= 0.0){ return 100.;}
        ppp = (pp < 1.0)? pp : 2. - pp;
        t = std::sqrt(-2.*std::log(ppp/2.));
        x = -0.70711*((2.30753+t*0.27061)/(1.+t*(0.99229+t*0.04481)) - t);
        for (int j=0;j<2;j++) {
                double err = std::erfc(x) - ppp;
                x += err/(1.12837916709551257*std::exp(-(x*x))-x*err);
        }
        return (pp < 1.0? x : -x);
};

__attribute((const)) double inverf(const double pp) {
    return inverfc(1.-pp);
};

/********************************************************************************
* Get a lornomal PP diameter
********************************************************************************/
double Get_Lognormal_random_Dp(const double Dp_geo,
                               const double Dp_sigmaG)
{
    if (Dp_sigmaG < 1.000000000000001)
    {
        return Dp_geo;
    }
    // Tails of the distribution are withdrawn
    // 2 sigmas -> 95.5% of the distribution
    double min_Dp = Dp_geo / (std::pow(Dp_sigmaG, 2));
    double max_Dp = Dp_geo * (std::pow(Dp_sigmaG, 2));
    double new_Dp(0.0);
    while ((new_Dp < min_Dp) || (new_Dp > max_Dp))
    {
        double x = rand_01();
        new_Dp = std::exp(std::log(Dp_geo) + std::sqrt(2.0) * std::log(Dp_sigmaG) * inverf(2.0 * x - 1.0)); //Log-normal
    }
    return new_Dp;
};

/********************************************************************************
* Cross product between two vectors
********************************************************************************/
std::array<double, 3> cross(const std::array<double, 3> a,
                            const std::array<double, 3> b){
    std::array<double, 3> cross_p;
    cross_p[0] = a[1] * b[2] - a[2] * b[1];
    cross_p[1] = a[2] * b[0] - a[0] * b[2];
    cross_p[2] = a[0] * b[1] - a[1] * b[0];
    return cross_p;
}

/********************************************************************************
* Inverse cos function (avoid numerical problems for slightly larger than 1 or
* slightly smaller than -1)
********************************************************************************/
double safe_acos(double value) {
    if (value < -1.0) {
        return pi;
    } else if (value > 1.0) {
        return 0;
    } else {
        return std::acos(value);
    }
}

/********************************************************************************
* GeometricMean
********************************************************************************/
double GeometricMean(const std::vector<double> vals)
{
    double geo_mean(0.0);
    for (size_t i = 0; i < vals.size(); i++)
    {
        geo_mean += std::log(vals[i]);
    }
    geo_mean = std::exp(geo_mean / static_cast<double>(vals.size()));
    return geo_mean;
}
}

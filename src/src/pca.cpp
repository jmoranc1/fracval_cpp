/*
    fracval_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "pca.hpp"

using namespace std;

namespace fracval
{
    PCA::PCA() : Df(0.0),
                 kf(0.0),
                 Dpp_geo_calculated(0.0),
                 Dpp_geo(0.0),
                 Dpp_gsd(0.0),
                 tol_ov(0.0),
                 Npp(0),
                 Npp_current(0),
                 pca_not_able(false),
                 show_error_warnings(false),
                 Gamma_pc(0.0),
                 theta_a(0.0),
                 x0(0.0),
                 y0(0.0),
                 z0(0.0),
                 r0(0.0),
                 current_overlapping(0.0),
                 Gamma_real(false),
                 Gamma_too_large(false),
                 sticking_successful(false),
                 current_total_aggregated_candidates(0),
                 current_total_NONaggregated_candidates(0),
                 aggregated_candidate(-1)
    {
    }

    /********************************************************************************
    * Generate a cluster by particle-cluster agglomeration
    * Input params: Npp,Df,kf,Rpp_geo,Rpp_gsd
    * Return: the sphere list
    ********************************************************************************/
    std::vector<Sphere> PCA::Create_pca_cluster(const size_t Npp_input,
                                                const double Df_input,
                                                const double kf_input,
                                                const double Dpp_geo_input,
                                                const double Dpp_gsd_input,
                                                const double tol_ov_input)
    {
        // 0. Save parameters
        Npp = Npp_input;
        Df = Df_input;
        kf = kf_input;
        Dpp_geo = Dpp_geo_input;
        Dpp_gsd = Dpp_gsd_input;
        tol_ov = tol_ov_input;

        return Create_pca_cluster();
    }

    std::vector<Sphere> PCA::Create_pca_cluster()
    {
        // 1. Initialize monomers according to PPSD (Rpp_geo,Rpp_gsd)
        std::vector<Sphere> monomers = Initialize_monomers();

        if (check_input_params_pca(Npp, Df, kf, Dpp_geo, Dpp_gsd, tol_ov))
        {
            Error(ErrorCodes::INPUT_ERROR, show_error_warnings);
            pca_not_able = true;
            return monomers;
        }

        for (size_t i=0; i < Npp; i++)
        {
            double diameter = Get_Lognormal_random_Dp(Dpp_geo, Dpp_gsd);
            monomers[i].Copy_sphere(Sphere(diameter,i));
        };
        Dpp_geo_calculated = GeometricMean(Extract_list_diameter(monomers));
        First_two_monomers(monomers);

        if (Npp < 3)
        {
            return monomers;
        }

        // 2. Create and update the object aggregate
        Aggregate aggregate;
        aggregate.Initialize(Npp_current, Dpp_geo_calculated, kf, Df, monomers);

        Aggregate aggregate_next;
        aggregate_next.Aggregate_copy(aggregate);

        // 3. Iteratively add one monomer to the growing aggregate
        while (Npp_current < Npp)
        {
            size_t Npp_next = Npp_current+1;
            size_t index_next = Npp_next - 1;
            Sphere sphere_next;
            sphere_next.Copy_sphere(monomers[index_next]);
            aggregate_next.Update_next(Npp_next, Df, kf, Dpp_geo_calculated, sphere_next);

            // Determine gamma
            Gamma_calculation(aggregate, aggregate_next, sphere_next);            

            // Generate a list of free monomers (NON aggregated) to be considered for aggregation
            Set_NONaggregated_candidates_list(monomers, index_next);

            // Generate a list of candidates from aggregate to be in contact with the new monomer
            Set_aggregated_candidates_list(monomers, sphere_next, aggregate);

            if ((current_total_aggregated_candidates == 0) && (current_total_NONaggregated_candidates == 0))
            {
                pca_not_able = true;
                return monomers;
            }

            sticking_successful = false;
            aggregated_candidate = -1;
            while ((!sticking_successful) && (current_total_aggregated_candidates > 0))
            {
                // pick an aggregated candidate and reset Nonaggregated candidates list
                aggregated_candidate = Aggregated_list_pick_one(monomers, index_next);

                if (aggregated_candidate < 0)
                {
                    break;
                }

                // sticking process and overlap check
                Sticking_process(monomers[aggregated_candidate], aggregate, sphere_next, monomers);

                while ((!sticking_successful) && (current_total_NONaggregated_candidates > 0))
                {
                    // pick a candidate from NON aggregated candidates
                    NONaggregated_list_pick_one(index_next, aggregate, aggregate_next, monomers, sphere_next);

                    // sticking process and overlap check
                    Sticking_process(monomers[aggregated_candidate], aggregate, sphere_next, monomers);

                    size_t trial(1);
                    while ((!sticking_successful) && (trial < Total_trial_rotations))
                    {
                        Sticking_process_try_again(aggregate, sphere_next, monomers);
                        trial++;
                    }
                }
            }

            if(Gamma_too_large)
            {
                Error(ErrorCodes::GAMMA_TOO_LARGE, show_error_warnings);
                pca_not_able = true;
                return monomers;
            }

            if ((current_total_aggregated_candidates == 0) &&
                (current_total_NONaggregated_candidates == 0) && (!sticking_successful))
            {
                Error(ErrorCodes::CANDIDATES_DEPLETED, show_error_warnings);
                pca_not_able = true;
                return monomers;
            } else if (!sticking_successful)
            {
                Error(ErrorCodes::UNKNOWN_ERROR, show_error_warnings);
                pca_not_able = true;
                return monomers;
            }

            // Update the monomers list
            monomers[index_next].Copy_sphere(sphere_next);
            // Reset status lists
            Reset_list_status(monomers);
            // Update the growing aggregate
            aggregate.Update_pca(Dpp_geo_calculated, kf, Df, monomers);
            Npp_current++;
        };

        return monomers;
    }

    /********************************************************************************
    * Random position for the first two monomers in contact
    ********************************************************************************/
    void PCA::First_two_monomers(std::vector<Sphere> &monomers)
    {
        double theta, phi;
        Random_point_sphere(theta, phi);
        double sum_radii = monomers[0].Radius + monomers[1].Radius;
        monomers[1].x = monomers[0].x + sum_radii * std::cos(theta) * std::sin(phi);
        monomers[1].y = monomers[0].y + sum_radii * std::sin(theta) * std::sin(phi);
        monomers[1].z = monomers[0].z + sum_radii * std::cos(phi);

        Npp_current = 2;
    }

    /********************************************************************************
    * Determine the value of the gamma distance between clusters
    ********************************************************************************/
    void PCA::Gamma_calculation(Aggregate &aggregate,
                                const Aggregate aggregate_next,
                                const Sphere sphere_next)
    {
        double m1 = static_cast<double>(aggregate.Npp);
        double m2 = 1.0;
        double m3 = static_cast<double>(aggregate_next.Npp);

        double rg1 = aggregate.rg;
        double rg2 = sphere_next.rg;
        double rg3 = std::max(aggregate_next.rg, aggregate.rg);

        double temp = std::pow(m3, 2) * std::pow(rg3, 2) - m3 * (m1 * std::pow(rg1, 2) + m2 * std::pow(rg2, 2));

        if (temp > 0.0)
        {
            Gamma_pc = std::sqrt(temp / (m1 * m2));
            Gamma_real = true;
        }
        else
        {
            Gamma_pc = 0.0;
            Gamma_real = false;
        }

        double gamma_rmax = Gamma_pc/(aggregate.rmax+0.5*Dpp_geo_calculated);
        aggregate.gamma_rmax = gamma_rmax;

        if (gamma_rmax > 1.0)
        {
            if (Npp_current < Np_max_Gamma_Rmax)
            {
                Gamma_pc = aggregate.rmax;
            }
            else
            {
                Gamma_pc = 0.0;
                Gamma_real = false;
                Gamma_too_large = true;
            }
        }
    }

    /********************************************************************************
    * Generate a list of free monomers to be considered for aggregation
    ********************************************************************************/
    void PCA::Set_aggregated_candidates_list(std::vector<Sphere> &monomers,
                                             const Sphere sphere_next,
                                             Aggregate &aggregate)
    {
        // First, reset the status of candidates
        for (size_t i=0; i < Npp_current; i++)
        {
            monomers[i].Aggregated_monomer_available = false;
        }

        current_total_aggregated_candidates = 0;

        // Second, determine current status of each monomer
        if (Gamma_real)
        {
            size_t counter = 0;
            for (size_t i=0; i < Npp_current; i++)
            {
                double dist = aggregate.Distance_cm(monomers[i]);
                monomers[i].distance_cm = dist;
                aggregate.my_monomers[i].Copy_sphere(monomers[i]);
                double R_sum = sphere_next.Radius + monomers[i].Radius;

                if ((dist > Gamma_pc - R_sum) && (dist < Gamma_pc + R_sum))
                {
                    monomers[i].Aggregated_monomer_available = true;
                    counter++;
                }

                if (R_sum > Gamma_pc)
                {
                    monomers[i].Aggregated_monomer_available = false;
                }
            }
            current_total_aggregated_candidates = counter;
        }
    }

    /********************************************************************************
    * Generate a list of candidates from aggregate to be in contact with the new monomer
    ********************************************************************************/
    void PCA::Set_NONaggregated_candidates_list(std::vector<Sphere> &monomers,
                                                size_t index_start)
    {
        size_t counter = 0;
        for (size_t i=index_start; i<Npp; i++)
        {
                monomers[i].NONaggregated_monomer_selected = false;
                counter++;
        }
        current_total_NONaggregated_candidates = counter;
    }

    /********************************************************************************
    * Pick a new candidate from considered_list
    ********************************************************************************/
    void PCA::NONaggregated_list_pick_one(const size_t index_next,
                                          Aggregate &aggregate,
                                          Aggregate &aggregate_next,
                                          std::vector<Sphere> &monomers,
                                          Sphere &sphere_next)
    {
        monomers[index_next].NONaggregated_monomer_selected = true;
        current_total_NONaggregated_candidates--;

        if (current_total_NONaggregated_candidates > 0)
        {
            size_t selected_one = Random_integer(current_total_NONaggregated_candidates);
            int selected_next_new(-1);

            bool found(false);
            size_t i(index_next), counter(0);
            while ((found == false) && (i < Npp))
            {
                if (!monomers[i].NONaggregated_monomer_selected)
                {
                    if (selected_one == counter)
                    {
                        found = true;
                        selected_next_new = i;
                    }
                    counter++;
                }
                i++;
            }

            Sphere sphere_next_old;
            sphere_next_old.Copy_sphere(sphere_next);
            monomers[index_next].Copy_sphere(monomers[selected_next_new]);
            monomers[selected_next_new].Copy_sphere(sphere_next_old);

            // Update aggregate_next and sphere_next
            sphere_next.Copy_sphere(monomers[index_next]);
            aggregate_next.Update_next(Npp_current + 1, Df, kf, Dpp_geo_calculated, sphere_next);

            // Recalculate Gamma and Aggregated_list
            Gamma_calculation(aggregate, aggregate_next, sphere_next);
            Set_aggregated_candidates_list(monomers, sphere_next, aggregate);
        }
    }

    /********************************************************************************
    * Pick a new candidate from Aggregated_list
    ********************************************************************************/
    int PCA::Aggregated_list_pick_one(std::vector<Sphere> &monomers,
                                      const size_t index_next)
    {
        // Reset NONaggregated candidates list
        Set_NONaggregated_candidates_list(monomers, index_next);

        if (aggregated_candidate >= 0)
        {
            monomers[aggregated_candidate].Aggregated_monomer_available = false;
            current_total_aggregated_candidates--;
        }

        if (current_total_aggregated_candidates > 0)
        {
            size_t selected_one = Random_integer(current_total_aggregated_candidates);
            size_t i(0), counter(0);
            while (i < Npp_current)
            {
                if (monomers[i].Aggregated_monomer_available)
                {
                    if (selected_one == counter)
                    {
                        return i;
                    }
                    else
                    {
                        counter++;
                    }
                }
                i++;
            }
        }

        return -1;
    }

    /********************************************************************************
    * Reset the availability status of all monomers
    ********************************************************************************/
    void PCA::Reset_list_status(std::vector<Sphere> &monomers)
    {
        for (auto &monomer: monomers)
        {
            monomer.NONaggregated_monomer_selected = false;
            monomer.Aggregated_monomer_available == false;
        }
        current_total_aggregated_candidates = 0;
        current_total_NONaggregated_candidates = 0;
    }

    /********************************************************************************
    * Sticking two agglomerates
    * To understand this function please refer to Appenix A (Sphere-sphere intersection) of
    * Morán, J., Fuentes, A., Liu, F., & Yon, J. (2019). Computer Physics Communications, 239, 225-237.
    * Parameters
    * (x1, y1, z1) -> center sphere 1: SELECTED (from growing aggregate 1), radius  R_sel + R_k
    * (z2, y2, z2) -> center sphere 2: Ceter of mass of growing aggregate 1, radius -> Gamma_pc
    ********************************************************************************/
    void PCA::Sticking_process(const Sphere slected_sph,
                               const Aggregate aggregate,
                               Sphere &sphere_next,
                               const std::vector<Sphere> monomers)
    {
        double x1 = slected_sph.x;
        double y1 = slected_sph.y;
        double z1 = slected_sph.z;
        double r1 = slected_sph.Radius + sphere_next.Radius;

        double x2 = aggregate.x;
        double y2 = aggregate.y;
        double z2 = aggregate.z;
        double r2 = Gamma_pc;

        // plane intersection both spheres: Ax + By + Cz + D = 0
        double A = 2. * (x2 - x1);
        double B = 2. * (y2 - y1);
        double C = 2. * (z2 - z1);
        double D = std::pow(x1, 2) - std::pow(x2, 2) +
                   std::pow(y1, 2) - std::pow(y2, 2) +
                   std::pow(z1, 2) - std::pow(z2, 2) -
                   std::pow(r1, 2) + std::pow(r2, 2);

        double t_sp = (x1*A + y1*B + z1*C + D) / (A*(x1-x2) + B*(y1-y2) + C*(z1-z2));

        x0 = x1 + t_sp*(x2-x1);
        y0 = y1 + t_sp*(y2-y1);
        z0 = z1 + t_sp*(z2-z1);

        double distanc = std::sqrt(std::pow(x2-x1,2) + std::pow(y2-y1,2) + std::pow(z2-z1,2));

        double temp = (std::pow(r1, 2) + std::pow(distanc, 2) - std::pow(r2, 2)) / (2. * r1 * distanc);
        double alpha_0 = safe_acos(temp);

        r0 = r1 * std::sin(alpha_0);

        double AmBdC = (A + B) / C;

        std::array<double, 3> k_vec;
        double k_vec_norm = std::sqrt(std::pow(A, 2) + std::pow(B, 2) + std::pow(C, 2));
        double i_vec_norm = std::sqrt(1. + 1. + std::pow(AmBdC, 2));

        k_vec = {A / k_vec_norm, B / k_vec_norm, C / k_vec_norm};        //vector perpendicular to plane Ax+By+Cz+D=0
        i_vec = {1. / i_vec_norm, 1. / i_vec_norm, -AmBdC / i_vec_norm}; //I've set x=1 and y=1 and solved Ax+By+Cz=0 to find z perpendicular
        j_vec = cross(k_vec, i_vec);

        double u = rand_01();
        theta_a = 2. * pi * u;

        // parametrized
        sphere_next.x = x0 + r0 * std::cos(theta_a) * i_vec[0] +
                        r0 * std::sin(theta_a) * j_vec[0];
        sphere_next.y = y0 + r0 * std::cos(theta_a) * i_vec[1] +
                        r0 * std::sin(theta_a) * j_vec[1];
        sphere_next.z = z0 + r0 * std::cos(theta_a) * i_vec[2] +
                        r0 * std::sin(theta_a) * j_vec[2];

        // Check overlapping
        Overlapping_check(sphere_next, monomers);
    }

    /********************************************************************************
    * Sticking two agglomerates
    * One monomer just slides on the surface of a neighbor to search for new configurations
    ********************************************************************************/
    void PCA::Sticking_process_try_again(const Aggregate aggregate,
                                         Sphere &sphere_next,
                                         const std::vector<Sphere> monomers)
    {
        double u = rand_01();
        theta_a = 2. * pi * u;

        // parametrized
        sphere_next.x = x0 + r0 * std::cos(theta_a) * i_vec[0] + r0 * std::sin(theta_a) * j_vec[0];
        sphere_next.y = y0 + r0 * std::cos(theta_a) * i_vec[1] + r0 * std::sin(theta_a) * j_vec[1];
        sphere_next.z = z0 + r0 * std::cos(theta_a) * i_vec[2] + r0 * std::sin(theta_a) * j_vec[2];

        // Check overlapping
        Overlapping_check(sphere_next, monomers);
    }

    /********************************************************************************
    * Check for overlapping between the added monomer and neighbors
    ********************************************************************************/
    void PCA::Overlapping_check(const Sphere sphere_next,
                                const std::vector<Sphere> monomers)
    {
        double distanc_ik, Radius_sum;
        double overlapping_max(0.0);
        for (size_t i = 0; i < Npp_current; i++)
        {
            distanc_ik = std::sqrt(std::pow(sphere_next.x - monomers[i].x, 2) +
                                   std::pow(sphere_next.y - monomers[i].y, 2) +
                                   std::pow(sphere_next.z - monomers[i].z, 2));
            Radius_sum = sphere_next.Radius + monomers[i].Radius;
            double overlapping = (Radius_sum - distanc_ik) / Radius_sum;

            // Check point-touching condition
            if (monomers[i].index == monomers[aggregated_candidate].index)
            {
                overlapping = 0.0;
            }

            if (overlapping > overlapping_max)
            {
                overlapping_max = overlapping;
            }
        }
        if (overlapping_max < tol_ov)
        {
            sticking_successful = true;
        }
        current_overlapping = overlapping_max;
    }

    /********************************************************************************
    * Return a vector with all the PP radii
    ********************************************************************************/
    std::vector<double> PCA::Extract_list_diameter(const std::vector<Sphere> monomers)
    {
        std::vector<double> list;
        for (size_t i = 0; i < Npp; i++)
        {
            list.push_back(monomers[i].Diameter);
        }
        return list;
    }

    /********************************************************************************
    * Initialize the list of monomers
    ********************************************************************************/
    std::vector<Sphere> PCA::Initialize_monomers()
    {
        std::vector<Sphere> monomers;
        monomers.resize(Npp);
        for (size_t i=0; i < Npp; i++)
        {
            monomers[i] = Sphere();
        };
        return monomers;
    }
}

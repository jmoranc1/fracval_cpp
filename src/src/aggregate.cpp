/*
    fracval_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "aggregate.hpp"
using namespace std;

namespace fracval{
Aggregate::Aggregate():
    rg(0.0),
    rg_theoric(0.0),
    rmax(0.0),
    gamma_rmax(0.0),
    Volume(0.0),
    Surface(0.0),
    x(0.0),
    y(0.0),
    z(0.0),
    dp(0.0),
    Npp(0),
    agglomerate_id(0),
    idx_start_end_monomers(),
    my_monomers(),
    available(true)
{
}

void Aggregate::Aggregate_copy(const Aggregate agg){
    rg = agg.rg;
    rg_theoric = agg.rg_theoric;
    rmax = agg.rmax;
    gamma_rmax = agg.gamma_rmax;
    Volume = agg.Volume;
    Surface = agg.Surface;
    x = agg.x;
    y = agg.y;
    z = agg.z;
    dp = agg.dp;
    Npp = agg.Npp;
    agglomerate_id = agg.agglomerate_id;
    idx_start_end_monomers = agg.idx_start_end_monomers;
    my_monomers = agg.my_monomers;
    available = agg.available;
}

void Aggregate::print() {
    std::cout << " Aggregate properties   " << std::endl;
    std::cout << "    available         : " << available << std::endl;
    std::cout << "    id                : " << agglomerate_id << std::endl;
    std::cout << "    Np                : " << Npp << std::endl;
    std::cout << "    Gyration radius   : " << rg << std::endl;
    std::cout << "    Theo. Gyr. radius : " << rg_theoric << std::endl;
    std::cout << "    Maximum radius    : " << rmax << std::endl;
    std::cout << "    Volume            : " << Volume << std::endl;
    std::cout << "    Surface           : " << Surface << std::endl;
    std::cout << "    Position          : " << x << " " << y << " " << z << std::endl;
    std::cout << "    dp                : " << dp << std::endl;
    std::cout << "    my_monomers.size(): " << my_monomers.size() << std::endl;
    std::cout << "    gamma/rmax        : " << gamma_rmax << std::endl;
    std::cout << "    monomers index    : " << idx_start_end_monomers[0] << " "
              << idx_start_end_monomers[1] << std::endl;
}

void Aggregate::Initialize(const size_t Npp_new,
                           const double Dpp_geo_calculated,
                           const double kf,
                           const double Df,
                           std::vector<Sphere> &monomers)
{
    Npp = Npp_new;
    Volume = 0.0;
    Surface = 0.0;
    x = 0.0;
    y = 0.0;
    z = 0.0;
    rg_theoric = 0.5*Dpp_geo_calculated*std::pow(static_cast<double>(Npp)/kf, 1.0/Df);
    my_monomers.resize(Npp);
    for (size_t i = 0; i < Npp; i++)
    {
        Volume += monomers[i].Volume;
        Surface += monomers[i].Surface;
        my_monomers[i].Copy_sphere(monomers[i]);
    }
    idx_start_end_monomers = {0, Npp-1};
    agglomerate_id = 0;
    available = true;

    // Update center of mass
    Center_mass();

    // Update radius of gyration
    Radius_gyration();
}

void Aggregate::Update_pca(const double Dpp_geo_calculated,
                           const double kf,
                           const double Df,
                           std::vector<Sphere> &monomers)
{
    Npp += 1;
    rg_theoric = 0.5*Dpp_geo_calculated*std::pow(static_cast<double>(Npp)/kf, 1.0/Df);
    my_monomers.resize(Npp);

    Volume = 0.0;
    Surface = 0.0;
    for (size_t i=0; i<Npp; i++)
    {
        my_monomers[i].Copy_sphere(monomers[i]);
        Volume += monomers[i].Volume;
        Surface += monomers[i].Surface;
    }
    idx_start_end_monomers = {0, Npp-1};
    agglomerate_id = 0;
    available = true;

    // Update center of mass
    Center_mass();
    
    // Update radius of gyration
    Radius_gyration();
}

void Aggregate::Update_next(const size_t Npp_new,
                            const double Df,
                            const double kf,
                            const double Dpp_geo,
                            const Sphere sph_next)
{
    Npp = Npp_new;
    rg = 0.5 * Dpp_geo * std::pow(static_cast<double>(Npp) / kf, 1.0 / Df);
    Volume += sph_next.Volume;
    Surface += sph_next.Surface;
    idx_start_end_monomers = {0, Npp-1};
}

void Aggregate::Center_mass()
{
    double xcm = 0.;
    double ycm = 0.;
    double zcm = 0.;
    double sum_volume(0.);

    for (const auto &monomer : my_monomers)
    {
        xcm += monomer.x * monomer.Volume;
        ycm += monomer.y * monomer.Volume;
        zcm += monomer.z * monomer.Volume;

        sum_volume += monomer.Volume;
    }
    xcm /= sum_volume;
    ycm /= sum_volume;
    zcm /= sum_volume;

    // Re-center the aggregate
    x = xcm;
    y = ycm;
    z = zcm;
}

void Aggregate::Radius_gyration()
{
    double rxcm, rycm, rzcm;
    double rcm = 0.0;
    double sum_volume = 0.0;
    double dist(0.0);

    for (auto &monomer : my_monomers)
    {
        rxcm = std::pow(monomer.x - x, 2);
        rycm = std::pow(monomer.y - y, 2);
        rzcm = std::pow(monomer.z - z, 2);

        rcm += monomer.Volume * (rxcm + rycm + rzcm + 0.6 * std::pow(monomer.Radius, 2));
        sum_volume += monomer.Volume;

        // Just to update rmax
        dist = std::sqrt(rxcm + rycm + rzcm);
        monomer.distance_cm = dist;
        if (dist + monomer.Radius > rmax)
        {
            rmax = dist + monomer.Radius;
        }
    };

    rg = std::sqrt(rcm / sum_volume);
}

double Aggregate::Distance_cm(const Sphere sph)
{
    double rxcm = std::pow(sph.x - x, 2);
    double rycm = std::pow(sph.y - y, 2);
    double rzcm = std::pow(sph.z - z, 2);

    return std::sqrt(rxcm + rycm + rzcm);
}

} // fracval namespace

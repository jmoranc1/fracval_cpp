/*
    fracval_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "sphere.hpp"
using namespace std;

namespace fracval{
Sphere::Sphere():
    Diameter(0.0),
    Radius(0.0),
    rg(0.0),
    Volume(0),
    Surface(0),
    x(0),
    y(0),
    z(0),
    distance_cm(0.0),
    Aggregated_monomer_available(false),
    NONaggregated_monomer_selected(false),
    index(0),
    agglomerate_id(0)
{
}

Sphere::Sphere(const double diameter,
               const size_t new_index)
{
    Diameter = diameter;
    Radius = 0.5 * Diameter;
    rg = std::sqrt(0.6) * Radius;
    Volume = (pi / 6.0) * std::pow(Diameter, 3);
    Surface = pi * std::pow(Diameter, 2);
    x = 0.0;
    y = 0.0;
    z = 0.0;
    distance_cm = 0.0;
    Aggregated_monomer_available = false;
    NONaggregated_monomer_selected = false;
    index = new_index;
    agglomerate_id = 0;
}

void Sphere::Copy_sphere(const Sphere sph){
    Diameter = sph.Diameter;
    Radius = sph.Radius;
    rg = sph.rg;
    Volume = sph.Volume;
    Surface = sph.Surface;
    x = sph.x;
    y = sph.y;
    z = sph.z;
    distance_cm = sph.distance_cm;
    Aggregated_monomer_available = sph.Aggregated_monomer_available;
    NONaggregated_monomer_selected = sph.NONaggregated_monomer_selected;
    index = sph.index;
    agglomerate_id = sph.agglomerate_id;
}

void Sphere::Set_agglomerate_id(const size_t new_agglomerate_id)
{
    agglomerate_id = new_agglomerate_id;
}

void Sphere::print() {
    std::cout << " Sphere properties   " << std::endl;
    std::cout << "    index                     : " << index << std::endl;
    std::cout << "    Gyration radius           : " << rg << std::endl;
    std::cout << "    Radius                    : " << Radius << std::endl;
    std::cout << "    Diameter                  : " << Diameter << std::endl;
    std::cout << "    Volume                    : " << Volume << std::endl;
    std::cout << "    Surface                   : " << Surface << std::endl;
    std::cout << "    Position                  : " << x << " " << y << " " << z << std::endl;
    std::cout << "    distance_cm               : " << distance_cm << std::endl;
    std::cout << "    Aggregated_list_status    : " << Aggregated_monomer_available << std::endl;
    std::cout << "    NONaggregated_list_status : " << NONaggregated_monomer_selected << std::endl;
}

} // fracval namespace

#!/usr/bin/env python3
# coding=utf-8

#    fracval_cpp: A code to simulate the agglomeration of aerosol particles
#    Copyright (C) 2021  José Morán
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
Create a raspberry cluster
"""

import numpy as np
import pandas as pd
import math
import random
from tqdm import tqdm

#***********************************************************
# Raspberry functions
#***********************************************************
def new_sphere(theta,phi,r):
    d = {'x': [r*np.sin(theta)*np.cos(phi)],\
         'y': [r*np.sin(theta)*np.sin(phi)],\
         'z': [r*np.cos(theta)], 'r': [1]}
    new_sph = pd.DataFrame(data=d)
    return new_sph
def add_spheres_plane(n_sph,theta,r,spheres,phase):
    for i in range(n_sph):
        phi = (i+phase) * 2*np.pi/n_sph
        new_sph = new_sphere(theta,phi,r)
        spheres = pd.concat([spheres,new_sph],ignore_index=True);
    return spheres
def Generate_hollow_raspberry_shell(R_factor,spheres,z_ad):
    R = 1
    # Iteratively add new spheres
    r = R*R_factor
    #beta = 2.*np.arcsin(R/r)
    beta = 2.*np.arccos(1-R**2/r**2/2)
    #print(beta, " old: ", 2.*np.arcsin(R/r))
    n_zz = np.pi/beta
    n_z = math.floor(n_zz)+z_ad
    n_sph = 1
    # add the poles
    spheres = add_spheres_plane(n_sph,0,r,spheres,0)
    spheres = add_spheres_plane(n_sph,np.pi,r,spheres,0)
    # add the rest of spheres
    for i in range(1,n_z):
        if ((i+1)%2==0):
            phase = 0.5
        else:
            phase = 0
        theta = i * np.pi/n_z
        r_s = r*np.sin(theta)
        alpha = 2.*np.arcsin(R/r_s)
        n_sph = math.floor(2*np.pi/alpha)
        spheres = add_spheres_plane(n_sph,theta,r,spheres,phase)
    return spheres
def Generate_hollow_raspberry(R_factor,z_ad):
    """
    Generate a 2-dimensional array of spheres forming a
    raspberry shape.
    
    Parameters
    ----------
    R_factor : float
        The factor between the radius of the raspberry and
        the radius of a primary sphere
    z_ad : int
        Add additional z-planes (becareful it lead to overlapping
        between primary spheres)
    Returns
    -------
    spheres: Pandas DataFrame
        This dataframe contains the following columns [x,y,z,r]
    """
    # Initialize the dataframe
    cols = ["x","y","z","r"]
    spheres = pd.DataFrame(columns=cols)
    spheres = Generate_hollow_raspberry_shell(R_factor,spheres,z_ad)
    return spheres
def Generate_filled_rapberry(n_shells,z_ad):
    """
    Generate a 3-dimensional array of spheres forming a
    raspberry shape.
    
    Parameters
    ----------
    n_shells : int
        The number of shells forming the raspberry
    z_ad : int
        Add additional z-planes (becareful it lead to overlapping
        between primary spheres)
    Returns
    -------
    spheres: Pandas DataFrame
        This dataframe contains the following columns [x,y,z,r]
    """
    R = 1
    # Add the first central sphere
    d = {'x': [0], 'y': [0], 'z': [0], 'r': [R]}
    spheres = pd.DataFrame(data=d)
    # Iteratively add new spheres
    for k in range(n_shells):
        shell = k+1
        R_factor = 2*shell
        spheres = Generate_hollow_raspberry_shell(R_factor,spheres,z_ad)
    return spheres
def Check_overlapping(spheres,new_sph,overlapping_tol):
    overlapping = False
    x_ns = new_sph["x"]
    y_ns = new_sph["y"]
    z_ns = new_sph["z"]
    for i in range(len(spheres["x"])):
        dist = float(np.sqrt(np.power(spheres["x"].iloc[i]-x_ns,2)+\
               np.power(spheres["y"].iloc[i]-y_ns,2)+\
               np.power(spheres["z"].iloc[i]-z_ns,2)))
        sum_r = spheres["r"].iloc[i] + new_sph["r"].iloc[0]
        if (dist < sum_r*(1+overlapping_tol)):
            overlapping = True
            return overlapping
    return overlapping
def rand_point_sphere():
    # -- Generating a random direction --
    phirandom = random.uniform(0, 1)*np.pi*2.0;
    thetarandom = np.arccos(1.0-2.0*random.uniform(0, 1))
    return thetarandom, phirandom
def Generate_random_raspberry_shell(R_factor,spheres,n_spheres,overlapping_tol):
    R = 1
    # Iteratively add new spheres
    r = R*R_factor
    for i in tqdm(range(n_spheres)):
        overlapping = True
        while(overlapping):
            theta, phi = rand_point_sphere()
            new_sph = new_sphere(theta,phi,r)            
            overlapping = Check_overlapping(spheres,new_sph,overlapping_tol)
        spheres = spheres.append(new_sph)
    return spheres


#***********************************************************
# Manage data files
#***********************************************************
def Save_results(name_exp,all_data):
    """
    Save results in an output file.
    
    Parameters
    ----------
    name_exp : str
        The address where the file is saved. At the end it should
        contain the name and extension of the exported file
    all_data : numpy array
        The information to be saved
    """
    np.savetxt(name_exp, all_data)
    return 
def Format_export(spheres):
    x = spheres["x"].values
    y = spheres["y"].values
    z = spheres["z"].values
    pos = np.column_stack((x,y,z))
    pos = np.reshape(pos, (len(x),3))
    return pos

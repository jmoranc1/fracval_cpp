# coding=utf-8
# cython: language_level=3
# cython: initializedcheck=False
# cython: binding=True
# cython: nonecheck=False
# cython: boundscheck=False
# cython: wraparound=False

#    fracval_cpp: A code to simulate the agglomeration of aerosol particles
#    Copyright (C) 2021  José Morán
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
cimport numpy as np
from libcpp cimport bool

cdef extern from "sphere.hpp" namespace "fracval":
    cdef cppclass Sphere:
        Sphere(double diameter) except +
        Sphere() except +        
        Sphere(const Sphere &sph) except +
        double Radius
        double x
        double y
        double z
        
cdef class PySphere:
    cdef Sphere* thisptr
    def __cinit__(self):
        self.thisptr = new Sphere()
    def __dealloc__(self):
        del self.thisptr

cdef extern from "main.hpp" namespace "fracval":
    void pca_cluster(const int Npp_input,
                     const double Df_input,
                     const double kf_input,
                     const double Dpp_geo_input,
                     const double Dpp_gsd_input,
                     const double tol_ov_input,
                     const bool show_error_warnings_input,
                     double& X[],double& Y[],double& Z[],double& R[]) nogil    

def fracval_pca_cluster_wrapper(int Npp_input,
                        double Df_input,
                        double kf_input,
                        double Dpp_geo_input,
                        double Dpp_gsd_input,
                        double tol_ov_input,
                        bool show_error_warnings_input):
    cdef double[:] X = np.empty(Npp_input, dtype=np.double)
    cdef double[:] Y = np.empty(Npp_input, dtype=np.double)
    cdef double[:] Z = np.empty(Npp_input, dtype=np.double)
    cdef double[:] R = np.empty(Npp_input, dtype=np.double)

    with nogil:
        pca_cluster(Npp_input,
                    Df_input,
                    kf_input,
                    Dpp_geo_input,
                    Dpp_gsd_input,
                    tol_ov_input,
                    show_error_warnings_input,
                    &X[0],&Y[0],&Z[0],&R[0])

    return np.asarray(X),np.asarray(Y),np.asarray(Z),np.asarray(R)

cdef extern from "main.hpp" namespace "fracval":
    void cca_cluster(const int Npp_input,
                     const double Df_input,
                     const double kf_input,
                     const double Dpp_geo_input,
                     const double Dpp_gsd_input,
                     const double tol_ov_input,
                     const double Percent_Np_subcl,
                     const bool show_error_warnings_input,
                     double& X[],double& Y[],double& Z[],double& R[]) nogil    

def fracval_cca_cluster_wrapper(int Npp_input,
                        double Df_input,
                        double kf_input,
                        double Dpp_geo_input,
                        double Dpp_gsd_input,
                        double tol_ov_input,
                        double Percent_Np_subcl,
                        bool show_error_warnings_input):
    cdef double[:] X = np.empty(Npp_input, dtype=np.double)
    cdef double[:] Y = np.empty(Npp_input, dtype=np.double)
    cdef double[:] Z = np.empty(Npp_input, dtype=np.double)
    cdef double[:] R = np.empty(Npp_input, dtype=np.double)

    with nogil:
        cca_cluster(Npp_input,
                    Df_input,
                    kf_input,
                    Dpp_geo_input,
                    Dpp_gsd_input,
                    tol_ov_input,
                    Percent_Np_subcl,
                    show_error_warnings_input,
                    &X[0],&Y[0],&Z[0],&R[0])

    return np.asarray(X),np.asarray(Y),np.asarray(Z),np.asarray(R)

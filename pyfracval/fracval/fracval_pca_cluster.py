#!/usr/bin/env python3
# coding=utf-8

#    fracval_cpp: A code to simulate the agglomeration of aerosol particles
#    Copyright (C) 2021  José Morán
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
Create an aggregate by PCA
"""

from .fracval_main_wrapper import fracval_pca_cluster_wrapper

import numpy as np

def fracval_pca_cluster(Npp_input,
                        Df_input,
                        kf_input,
                        Dpp_geo_input,
                        Dpp_gsd_input,
                        tol_ov_input,
                        show_error_warnings_input):
    return fracval_pca_cluster_wrapper(Npp_input,
                    Df_input,
                    kf_input,
                    Dpp_geo_input,
                    Dpp_gsd_input,
                    tol_ov_input,
                    show_error_warnings_input)

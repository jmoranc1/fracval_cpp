#!/usr/bin/env python3
# coding=utf-8

#    fracval_cpp: A code to simulate the agglomeration of aerosol particles
#    Copyright (C) 2021  José Morán
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
Set of Python libraries for post-processing FracVAL data
"""
import os
from multiprocessing.pool import ThreadPool, Pool

from .plot import Plot_3d
from .fracval import fracval_pca_cluster
from .fracval import fracval_cca_cluster
from .fracval import Generate_filled_rapberry,Generate_hollow_raspberry

try:
    # noinspection PyUnresolvedReferences
    from .version import version
except ModuleNotFoundError:
    version = "Unknown"


__all__ = ["fracval_pca_cluster", "fracval_cca_cluster", "Plot_3d", "Generate_filled_rapberry", "Generate_hollow_raspberry"]

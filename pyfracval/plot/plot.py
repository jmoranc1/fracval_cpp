#!/usr/bin/env python3
# coding=utf-8

#    fracval_cpp: A code to simulate the agglomeration of aerosol particles
#    Copyright (C) 2021  José Morán
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
Plot an aggregate
"""

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

def plot_one_sphere(x,y,z,r,n,ax):
    u = np.linspace(0,2*np.pi,num=n)
    v = np.linspace(0,np.pi,num=n)

    X = x + r* np.outer(np.cos(u), np.sin(v))
    Y = y + r* np.outer(np.sin(u), np.sin(v))
    Z = z + r* np.outer(np.ones(np.size(u)), np.cos(v))

    # The rstride and cstride arguments default to 10
    ax.plot_surface(X,Y,Z, rstride=4, cstride=4)
    return

def Plot_monomers_3d(x,y,z,r,n=50):
    fig, ax = plt.subplots(figsize=(10,10))
    ax = fig.add_subplot(111, projection='3d')
    for i in range(len(x)):
        plot_one_sphere(x[i],y[i],z[i],r[i],n,ax)
    ax.set_xlabel('X position', fontsize=20)
    ax.set_ylabel('Y position', fontsize=20)
    ax.set_zlabel('Z position', fontsize=20)
    plt.show()
    return

def Plot_3d(monomers,n=50):
    """
    Plot an aggregate of primary spheres in 3d.
    
    Parameters
    ----------
    monomers : array or pandas dataframe
        array of columns [x,y,z,r].
    n : int
        The number of points to define the surface
        of spheres (50 by default).
    Returns
    -------
    Matplotlib.pyplot
        A 3-dimensional plot of the aggregate.
    """
    x = np.array(monomers["x"])
    x = x.flatten()
    y = np.array(monomers["y"])
    y = y.flatten()
    z = np.array(monomers["z"])
    z = z.flatten()
    r = np.array(monomers["r"])
    r = r.flatten()
    Plot_monomers_3d(x,y,z,r,n)
    return    

/*
    fracval_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SPHERE_H
#define SPHERE_H 1

#include "constants.hpp"

namespace fracval{
class Sphere{
    private:
    public:        
        double Diameter;                            // Sphere diameter
        double Radius;                              // Sphere radius
        double rg;                                  // Gyration radius
        double Volume;                              // Aggregate's volume
        double Surface;                             // Surface area of the aggregate
        double x,y,z;                               // Position of the center of mass
        double distance_cm;                         // Distance to the center of mass
        bool Aggregated_monomer_available;          // Is it candidate for agglomeration or not?
        bool NONaggregated_monomer_selected;        // Has this free monomer being selected for agglomeration yet?
        size_t index;
        size_t agglomerate_id;
        // Getters
        void print();
        // Modifiers
        void Copy_sphere(const Sphere sph);
        void Set_agglomerate_id(const size_t new_agglomerate_id);
        // Constructor
        Sphere();
        Sphere(const double diameter,
               const size_t new_index);
        // Destructor
        ~Sphere(){};
};
}

#endif // SPHERE_H

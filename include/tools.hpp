/*
    fracval_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TOOLS_HPP
#define TOOLS_HPP

#include "constants.hpp"

namespace fracval
{
    double rand_01();
    void Random_point_sphere(double &theta, double &phi);
    size_t Random_integer(const size_t n_max);
    __attribute((const)) double inverfc(const double pp);
    __attribute((const)) double inverf(const double pp);
    double Get_Lognormal_random_Dp(const double Dp_geo,
                                   const double Dp_sigmaG);
    std::array<double, 3> cross(const std::array<double, 3> a,
                                const std::array<double, 3> b);
    double safe_acos(double value);
    double GeometricMean(const std::vector<double> vals);
}

#endif // TOOLS_HPP

/*
    fracval_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef IO_H
#define IO_H

#include "constants.hpp"
#include <inipp.h>

using namespace std;

namespace fracval
{
    class Input_Output
    {
    public:
        size_t Npp_input;
        double Df_input;
        double kf_input;
        double Dpp_geo_input;
        double Dpp_gsd_input;
        double tol_ov_input;
        double Percent_Np_subcl_input;
        bool show_error_warning;
        Methods method;
    public:
        Input_Output();
        void Read_input_parameters(const std::string &input_parameters_file);
        Methods resolve_method(const std::string &method_str);
        void print();
    };
}

#endif // IO_H

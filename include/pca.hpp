/*
    fracval_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PCA_HPP
#define PCA_HPP
#include "tools.hpp"
#include "sphere.hpp"
#include "aggregate.hpp"
#include "exceptions.hpp"

#include<ctime>

namespace fracval
{
    class PCA
    {
    public:
        double Df, kf;
        double Dpp_geo_calculated;
        double Dpp_geo, Dpp_gsd;
        double tol_ov;
        size_t Npp;
        size_t Npp_current;
        bool pca_not_able;
        bool show_error_warnings;

    private:
        double Gamma_pc;
        double theta_a, x0, y0, z0, r0;
        std::array<double, 3> i_vec, j_vec;
        double current_overlapping;
        bool Gamma_real;
        bool Gamma_too_large;
        bool sticking_successful;
        size_t current_total_aggregated_candidates;
        size_t current_total_NONaggregated_candidates;
        int aggregated_candidate;

    public:
        PCA();
        std::vector<Sphere> Create_pca_cluster(const size_t Npp,
                                               const double Df,
                                               const double kf,
                                               const double Rpp,
                                               const double Rpp_gsd,
                                               const double tol_ov_input);
        std::vector<Sphere> Create_pca_cluster();
        void First_two_monomers(std::vector<Sphere> &monomers);
        void Gamma_calculation(Aggregate &aggregate,
                               const Aggregate aggregate_next,
                               const Sphere sphere_next);
        void Set_aggregated_candidates_list(std::vector<Sphere> &monomers,
                                            const Sphere sphere_next,
                                            Aggregate &aggregate);
        void Reset_list_status(std::vector<Sphere> &monomers);
        void Set_NONaggregated_candidates_list(std::vector<Sphere> &monomers,
                                               size_t index_start);
        void NONaggregated_list_pick_one(const size_t index_next,
                                         Aggregate &aggregate,
                                         Aggregate &aggregate_next,
                                         std::vector<Sphere> &monomers,
                                         Sphere &sphere_next);
        int Aggregated_list_pick_one(std::vector<Sphere> &monomers,
                                     const size_t index_next);
        void Sticking_process(const Sphere slected_sph,
                              const Aggregate aggregate,
                              Sphere &sphere_next,
                              const std::vector<Sphere> monomers);
        void Sticking_process_try_again(const Aggregate aggregate,
                                        Sphere &sphere_next,
                                        const std::vector<Sphere> monomers);
        void Overlapping_check(const Sphere sphere_next,
                               const std::vector<Sphere> monomers);
        std::vector<double> Extract_list_diameter(const std::vector<Sphere> monomers);
        std::vector<Sphere> Initialize_monomers();
    };
}

#endif // PCA_HPP

/*
    fracval_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef INCLUDE_EXCEPTIONS_HPP
#define INCLUDE_EXCEPTIONS_HPP
#include "constants.hpp"

namespace fracval
{
    void Error(const ErrorCodes error_code,
               const bool show_error_warnings);
    bool check_input_params_pca(const size_t Npp_input,
                                const double Df_input,
                                const double kf_input,
                                const double Dpp_geo_input,
                                const double Dpp_gsd_input,
                                const double tol_ov_input);
    bool check_input_params_cca(const size_t Npp_input,
                                const double Df_input,
                                const double kf_input,
                                const double Dpp_geo_input,
                                const double Dpp_gsd_input,
                                const double tol_ov_input,
                                const double Percent_Np_subcl_input);
}
#endif //INCLUDE_EXCEPTIONS_HPP

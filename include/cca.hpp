/*
    fracval_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CCA_HPP
#define CCA_HPP
#include "tools.hpp"
#include "sphere.hpp"
#include "aggregate.hpp"
#include "exceptions.hpp"
#include "pca.hpp"

#include <ctime>

namespace fracval
{
    class CCA
    {
    public:
        double Df, kf;
        double Dpp_geo, Dpp_gsd;
        double tol_ov;
        double Percent_Np_subcl;
        size_t Npp;
        size_t Np_subcl;
        bool cca_not_able;
        bool show_error_warnings;

    private:
        double Gamma_cc;
        double theta_a, x0, y0, z0, r0;
        std::array<double, 3> i_vec, j_vec;
        double current_overlapping;
        bool Gamma_real;
        size_t Number_clusters;
        size_t current_available_aggregates;
        size_t selected_cluster_1;
        size_t selected_cluster_2;

    public:
        CCA();
        std::vector<Sphere> Create_cca_cluster(const size_t Npp_input,
                                               const double Df_input,
                                               const double kf_input,
                                               const double Dpp_geo_input,
                                               const double Dpp_gsd_input,
                                               const double tol_ov_input,
                                               const double Percent_Np_subcl_input);
        std::vector<Sphere> Create_cca_cluster();
        bool PCA_subclusters(std::vector<Sphere> &monomers,
                             std::vector<Aggregate> &aggregates);
        size_t Initial_Np_subclusters();
        bool new_PCA_cluster(const size_t total_monomers,
                             const size_t Np_cluster,
                             std::vector<Sphere> &monomers,
                             const size_t id_new,
                             std::vector<Aggregate> &aggregates);
        std::vector<Sphere> Copy_monomers_list(const std::vector<Sphere> old_monomers);
        void Find_new_pair_subclusters(std::vector<Aggregate> &aggregates);
        bool Agglomerate_selected_pair_subclusters(std::vector<Aggregate> &aggregates,
                                                   std::vector<Sphere> &monomers_next);
        void Gamma_calculation(Aggregate &aggregate_next,
                               const Aggregate aggregate_1,
                               const Aggregate aggregate_2);
        Aggregate New_newt_aggregate(const Aggregate aggregate_1,
                                     const Aggregate aggregate_2);
    };
}

#endif // CCA_HPP

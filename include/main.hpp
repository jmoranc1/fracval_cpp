/*
    fracval_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MAIN_H
#define MAIN_H 1

#include "IO.hpp"
#include "cca.hpp"

int main(int argc, char *argv[]);

namespace fracval
{
    void pca_cluster(const size_t Npp_input,
                     const double Df_input,
                     const double kf_input,
                     const double Dpp_geo_input,
                     const double Dpp_gsd_input,
                     const double tol_ov_input,
                     const bool show_error_warnings_input,
                     double X[], double Y[], double Z[], double R[]);
    void pca_cluster(PCA pca,
                     double X[], double Y[], double Z[], double R[]);

    void cca_cluster(const size_t Npp_input,
                     const double Df_input,
                     const double kf_input,
                     const double Dpp_geo_input,
                     const double Dpp_gsd_input,
                     const double tol_ov_input,
                     const double Percent_Np_subcl_input,
                     const bool show_error_warnings_input,
                     double X[], double Y[], double Z[], double R[]);
    void cca_cluster(CCA cca,
                     double X[],
                     double Y[],
                     double Z[],
                     double R[]);
    void Seed_Random();
    size_t Initialize_pca(const Input_Output IO,
                          PCA &pca);
    size_t Initialize_cca(const Input_Output IO,
                          CCA &cca);
}

#endif // MAIN_H

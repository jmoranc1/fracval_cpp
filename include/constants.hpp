/*
    fracval_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CONSTANTS_HPP
#define CONSTANTS_HPP
#include <cmath>
#include <vector>
#include <array>
#include <iostream>
#include <string>

namespace fracval
{
    const double pi = 4.0 * std::atan(1.0); //!pi number 3.14
    const size_t Np_subcl_min = 5;
    const size_t Np_subcl_max = 50;
    const size_t Total_trial_rotations = 360;
    const size_t Total_trials_PCA_subcl = 200;
    const size_t Np_max_Gamma_Rmax = 5;

    enum ErrorCodes
    {
        NO_ERROR,
        UNKNOWN_ERROR,
        CANDIDATES_DEPLETED,
        GAMMA_TOO_LARGE,
        PCA_SUBCLUSTERS_NA,
        CCA_NA,
        INPUT_ERROR
    };

    enum Methods
    {
        CC_A,
        PC_A,
        UNKNOWN_METHOD
    };
    static const std::array<const std::string, 2> METHODS_STR{{"cca", "pca"}};

}

#endif // TCONSTANTS_HPP

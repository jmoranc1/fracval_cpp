/*
    fracval_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef AGGREGATE_H
#define AGGREGATE_H 1

#include "constants.hpp"
#include "sphere.hpp"

namespace fracval{
class Aggregate{
    private:
    public:
        double rg;                      // Gyration Radius
        double rg_theoric;              // Theoretical gyration Radius
        double rmax;                    // Maximum radius
        double gamma_rmax;              // Maximum radius divided by gamma
        double Volume;                  // Aggregate's volume
        double Surface;                 // Surface area of the aggregate
        double x,y,z;                   // Position of the center of mass
        double dp;                      // Average primary particle diameter
        size_t Npp;                     // Number of monomers
        size_t agglomerate_id;
        std::array<size_t, 2> idx_start_end_monomers; 
        std::vector<Sphere> my_monomers;
        bool available;
        // Getters
        void print();
        // Modifiers
        void Initialize(const size_t Npp_new,
                        const double Dpp_geo_calculated,
                        const double kf,
                        const double Df,
                        std::vector<Sphere> &monomers);
        void Update_pca(const double Dpp_geo_calculated,
                        const double kf,
                        const double Df,
                        std::vector<Sphere> &monomers);
        void Update_next(const size_t Npp_new,
                         const double Df,
                         const double kf,
                         const double Dpp_geo,
                         const Sphere sph_next);
        void Center_mass();
        void Radius_gyration();
        double Distance_cm(const Sphere sph);
        void Aggregate_copy(const Aggregate agg);
        // Constructor
        Aggregate();
        // Destructor
        ~Aggregate(){};
};
}

#endif // AGGREGATE_H
